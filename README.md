## coding-exercise

This script was developed as a home exercise.
The execution string will look like this:

`python3 certificate-parcer.py -H hostnames.txt -O output.txt -db $DB_NAME -dp $DB_PASSWORD -du $DB_USER -dh $DB_HOST`

Parameters description:

- -H - The path and name to file which content hosts like google.com or apple.com
- -O - The path and name to an output file with sorted certificates by date and with them urls
- -db - Database name
- -dp - Database password
- -du - Database user
- -dh - Database host or ip address

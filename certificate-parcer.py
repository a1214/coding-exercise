#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import argparse
import ssl, socket
from datetime import datetime
import mysql.connector
import uuid

# Fuction which open the file and read URL lines by loop 
# Then then the function open a socket and read certificate information
# Save certificate date to variable and add it to map
# The map will be returned after execution
def sort_certs_from_file(file_path):
    if file_path:
        with open(file_path, "r") as reader:
            site_certificates = {}
            for hostname in reader.readlines():
                hostname = hostname.strip("\n")
                ctx = ssl.create_default_context()
                with ctx.wrap_socket(socket.socket(), server_hostname=hostname) as s:
                    s.connect((hostname, 443))
                    cert = s.getpeercert()

                datetime_object = datetime.strptime(cert['notAfter'], '%b %d %H:%M:%S %Y %Z')
                site_certificates[hostname]=datetime_object

        sorted_site_certificates = sorted(site_certificates.items(), key=lambda x: x[1])
    return sorted_site_certificates

# This function writes certificate date to file.
# File name can be used from commandline parameter
def write_data_to_file(output_file_path, sorted_certs):
    if output_file_path:
        with open(output_file_path, "w") as writer:
            for i in sorted_certs:
                writer.write(str(i[0]) + " - " + str(i[1]) + "\n")
    else:
        print("Please put output file name in parameter")
# The function writes information to mysql database
# In this case, I use the default mysql connector and parameters from the command line
# Firstly, function checks table, if a table is not exist, then create a table
# Secondly, function insert data from sort_certs_from_file() function to db table
def write_data_to_db(db_host, db_user, db_pass, db_name, sorted_certs):
    if db_host and db_user and db_pass and db_name:
        mydb = mysql.connector.connect(
        host=db_host,
        user=db_user,
        password=db_pass,
        database=db_name
        )

        mycursor = mydb.cursor()

        table = """CREATE TABLE IF NOT EXISTS site_certificates (
                        id VARCHAR(255) NOT NULL,
                        site VARCHAR(255) NOT NULL,
                        certificate_date VARCHAR(255) NOT NULL)"""
        mycursor.execute(table)

        id = uuid.uuid1()
        for i in sorted_certs:
            val = (str(id), str(i[0]), str(i[1]))
            insert_cert_data = """INSERT INTO site_certificates (id, site, certificate_date) VALUES (%s, %s, %s)"""
            mycursor.execute(insert_cert_data, val)
        mydb.commit()
    else:
        print("Please put db parameters")

def main():
    parser = argparse.ArgumentParser(description='Script arguments')
    parser.add_argument("-H", '--hostnames', type=str, help='file with websites')
    parser.add_argument("-O", '--output', type=str, help='output file')
    parser.add_argument("-db", '--db_name', type=str, help='DB name')
    parser.add_argument("-du", '--db_user', type=str, help='DB user')
    parser.add_argument("-dp", '--db_password', type=str, help='DB password')
    parser.add_argument("-dh", '--db_host', type=str, help='DB host')

    arguments = parser.parse_args()
    certs = sort_certs_from_file(arguments.hostnames)
    write_data_to_file(arguments.output, certs)
    write_data_to_db(arguments.db_host, arguments.db_user, arguments.db_password, arguments.db_name, certs)

if __name__ == "__main__":
    main()
